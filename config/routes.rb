Spree::Core::Engine.add_routes do
  # Add your extension routes here
  # post '/razorpay/confirm', :to => "razorpay#confirm", :as => :confirm_razorpay
  resources :razorpay, only: :create
end
