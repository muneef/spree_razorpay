module Spree
  class Gateway::SpreeRazorpay < Gateway
    preference :key_id, :string
    preference :key_secret, :string
    preference :merchant_name, :string
    preference :merchant_description, :text
    preference :merchant_address, :string
    preference :theme_color, :string, default: "#F37254"

    def provider_class
      self
    end

    def payment_source_class
      Spree :SpreeRazorpayCheckout
    end

    def source_required?
      true
    end

    def method_type
      "razorpay"
    end

    def actions
      %w{capture void}
    end

    def supports?(source)
      true
    end

    def setup_razorpay(payment_method)
      Razorpay.setup(payment_method.preferences[:key_id], payment_method.preferences[:key_secret])
    end

    def payment(payment_object, payment_method, order)
      
      puts "ORDER 🧺 \n"
      puts order.inspect
      puts payment_object.inspect
      puts "ORDER 🧺 \n"

      _razorpay_checkout = Spree::RazorpayCheckout.new(
        order_id: order.id,
        razorpay_payment_id: payment_object.id,
        status: payment_object.status,
        payment_method: payment_object.method,
        card_id: payment_object.card_id,
        bank: payment_object.bank,
        wallet: payment_object.wallet,
        vpa: payment_object.vpa,
        email: payment_object.email,
        contact: payment_object.contact,
      )

      _razorpay_checkout.save

      order.payments.create!({
        payment_method: payment_method,
        amount: order.total,
        source: _razorpay_checkout,
      })
    end

    def client_token(params, payment_method, order)
      setup_razorpay(payment_method)
      payment_object = Razorpay::Payment.fetch(params[:razorpay_payment_id])
      payment = payment(payment_object, payment_method, order)

      status = payment_object.status

      if status == "authorized"
        payment_object.capture({ amount: order.amount_in_paise })
        puts order.inspect
        razorpay_pmnt_obj = Razorpay::Payment.fetch(params[:razorpay_payment_id])
        payment.update(response_code: razorpay_pmnt_obj.status)
        razorpay_pmnt_obj.status
        
      else
        raise StandardError, 'Unable to capture payment'
      end
    end

    # def payment(payment_object, payment_method)
    #   _razorpay_checkout = Spree:SpreeRazorpayCheckout.new(
    #     order_id: order.id,
    #     razorpay_payment_id: payment_object.id,
    #     status: payment_object.status,
    #     payment_method: payment_object.method,
    #     card_id: payment_object.card_id,
    #     bank: payment_object.bank,
    #     wallet: payment_object.wallet,
    #     vpa: payment_object.vpa,
    #     email: payment_object.email,
    #     contact: payment_object.contact
    #   )

    #   _razorpay_checkout.save

    #   _razorpay_payment = order.payments.create!({
    #       payment_method: payment_method,
    #       amount: order.total,
    #       source: _razorpay_checkout
    #     })

    # end
    #   # if status == "authorized"

    #   _razorpay_checkout = Spree:SpreeRazorpayCheckout.new(
    #     order_id: order.id,
    #     razorpay_payment_id: payment_object.id,
    #     status: payment_object.status,
    #     payment_method: payment_object.method,
    #     card_id: payment_object.card_id,
    #     bank: payment_object.bank,
    #     wallet: payment_object.wallet,
    #     vpa: payment_object.vpa,
    #     email: payment_object.email,
    #     contact: payment_object.contact
    #   )

    #     # # order.save

    #     # _razorpay_checkout.save

    #     # _razorpay_payment = order.payments.create!({
    #     #   payment_method: payment_method,
    #     #   amount: order.total,
    #     #   source: _razorpay_checkout
    #     # })
    # )

    # def self.version
    #   Gem::Specification.find_by_name('spree_razorpay').version.to_s
    # end

    # def cancel(charge_ari)
    #   _payment = Spree::Payment.valid.where(
    #     response_code: charge_ari,
    #     source_type:   payment_source_class.to_s
    #   ).first

    #   return if _payment.nil?

    #   if _payment.pending?
    #     _payment.void_transaction!
    #   elsif _payment.completed? && _payment.can_credit?
    #     provider.refund(_payment.credit_allowed.to_money.cents, charge_ari)
    #   end
    # end
    def purchase(amount, transaction_details, gateway_options = {})
      ActiveMerchant::Billing::Response.new(true, "razorpay success")
    end

    def request_type
      "DEFAULT"
    end
  end
end
