module Spree
    class RazorpayCheckout < ActiveRecord::Base
      has_one :payment, foreign_key: :source_id, as: :source, class_name: 'Spree::Payment'
      has_one :order, through: :payment
      
      after_commit :update_payment_and_order

      scope :with_payment_profile, -> { all }

      # scope :in_state, ->(state) { where(state: state) }
      # scope :not_in_state, ->(state) { where.not(state: state) }
  
      def name
        "Razorpay Checkout"
      end
  
      def details
        @details ||= payment_method.provider.get_checkout token
      end
  
      def actions
        %w{capture void}
      end
  
      # Indicates whether its possible to capture the payment
      def can_capture?(payment)
        (payment.pending? || payment.checkout?) && !payment.response_code.blank?
      end


      def update_payment_and_order
        puts "🐦 update_payment_and_order"
        puts payment.inspect
        puts changes.inspect
        # return unless (changes = previous_changes[:state])
        # return unless changes[0] != changes[1]
        # return unless payment
        
        # puts "🐦 update_payment_and_order"
        # puts order.inspect
        # utils = Gateway::BraintreeVzeroBase::Utils.new(Gateway::BraintreeVzeroBase.first, order)
        # payment_state = utils.map_payment_status(state)
        # payment.send("complete")
      end
  
      # Indicates whether its possible to void the payment.
      def can_void?(payment)
        !payment.void? && payment.pending? && !payment.response_code.blank?
      end

      def payment_action(state)
        case state
        when 'pending'
          'pend'
        when 'void'
          'void'
        when 'completed'
          'complete'
        else
          'failure'
        end
      end
    end
  end