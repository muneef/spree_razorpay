module Spree::OrderDecorator
    extend Spree::DisplayMoney

    def self.prepended(base)
      base.state_machine.before_transition to: :complete, do: :process_razorpay_payments
    end
   
    def amount_in_paise
      (amount.to_f * 100).to_i
    end

    def user_full_name
      secure object.user_full_name
    end

    def self.decorated_class
      Order
    end

    def process_razorpayment(params, order)
      "🔫"
    end
  
    def successful_payment
      paid? || payments.any? {|p| p.after_pay_method? && p.authorized?}
    end
  
    alias paid_or_authorized? successful_payment
  
    def authorized?
      payments.last.authorized?
    end

    def paid_with_razorpay?
      payments.valid.map(&:payment_method).compact.any? { |p| p.is_a?(Spree::Gateway::SpreeRazorpay) }
    end
    
    def invalidate_razorpay_payments
        return unless paid_with_razorpay?

        payments.valid.each do |payment|
          payment.invalidate! if payment.payment_method.is_a?(Spree::Gateway::SpreeRazorpay)
        end
      end

    def order_adjustment_total
      adjustments.eligible.sum(:amount)
    end
  
    def has_order_adjustments?
      order_adjustment_total.abs > 0
    end

    def process_payments!
      puts "🦈🦈🦈🦈"
          # puts payments.inspect
      # # Payments are processed in confirm_payment step where after successful
      # # 3D Secure authentication `intent_client_key` is saved for payment.
      # # In case authentication is unsuccessful, `intent_client_key` is removed.
      # return if intents? && payments.valid.last.intent_client_key.present?

      super
    end

    def process_razorpay_payments
        return unless paid_with_razorpay?
        

        if payments.valid.empty?
          puts "🦈🦈"
          puts payments.inspect
          # order.errors.add(:base, Spree.t(:no_payment_found))
          false
        else          
          # payments.valid.last.update(amount: total)
          process_payments!
        end
      end
  end
  
  Spree::Order.prepend(Spree::OrderDecorator)