module Spree
  class RazorpayController < StoreController
    # skip_before_action :authenticate_user

    before_action :find_order, only: :create

    def create
      gateway = if params[:payment_method_id]
          Spree::Gateway::SpreeRazorpay.find(params[:payment_method_id])
        else
          Spree::Gateway::SpreeRazorpay.active.first
        end
      
        response_status = gateway.client_token(params, payment_method, current_order)
      
      puts "🔖" 
      puts response_status.inspect
      @order = current_order

      if response_status == 'captured'
        @order.next
        @message = Spree.t(:order_processed_successfully)
        @current_order = nil
        flash.notice = Spree.t(:order_processed_successfully)
        flash['order_completed'] = true
        @error = false
        @redirect_path = order_path(@order)
      else
        @order.update_attributes(payment_state: 'failed')
        @error = true
        @message = 'There was an error processing your payment'
        @redirect_path = checkout_state_path(@order.state)
      end
    end

    def payment_method
      Spree::PaymentMethod.find(params[:payment_method_id])
    end

    private

    # We're skipping permission check for order, because it is needed only to get a currency
    def find_order
      @order = Spree::Order.find_by(number: params[:order_number])
    end
  end
end
